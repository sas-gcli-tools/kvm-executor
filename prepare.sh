#!/usr/bin/env bash

# /opt/libvirt-driver/prepare.sh

currentDir="$( cd "$( dirname "${BASH_SOURCE[0]}" )" >/dev/null 2>&1 && pwd )"
source ${currentDir}/base.sh # Get variables from base script.

set -eo pipefail

# trap any error, and mark it as a system failure.
trap "exit $SYSTEM_FAILURE_EXIT_CODE" ERR
#trap '(read -p "[$BASH_SOURCE:$LINENO] $BASH_COMMAND")' DEBUG
# Copy base disk to use for Job.
echo "I am running as $(whoami)"
echo "virsh uri is $(virsh uri)"
qemu-img create -f qcow2 -b "$BASE_VM_IMAGE" "$VM_IMAGE" -F qcow2 

# Install the VM
virt-install \
    --name "$VM_ID" \
    --os-variant win10 \
   --disk "$VM_IMAGE",format=qcow2,bus=virtio  \
    --import \
    --vcpus=2 
    --ram=8392 \
    --network model=virtio \
    --noautoconsole \
    --clock hpet-present=yes,hypervclock-present=yes
    --xml xpath.delete=./clock/timer[@name='rtc'],xpath.delete=./clock/timer[@name='pit']
#    --graphics none


# Wait for VM to get IP
echo 'Waiting for VM to get IP'
for i in $(seq 1 60); do
    VM_IP=$(_get_vm_ip)

    if [ -n "$VM_IP" ]; then
        echo "VM got IP: $VM_IP"
        break
    fi
    echo $i
    if [ "$i" == "180" ]; then
        echo 'Waited 180 seconds for VM to start, exiting...'
        # Inform GitLab Runner that this is a system failure, so it
        # should be retried.
        exit "$SYSTEM_FAILURE_EXIT_CODE"
    fi

    sleep 1s
done

# Wait for ssh to become available
echo "Waiting for sshd to be available at $SSH_USER@$VM_IP"
nc -vzw 30 "$VM_IP" 22
