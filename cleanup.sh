#!/usr/bin/env bash

# /opt/libvirt-driver/cleanup.sh

currentDir="$( cd "$( dirname "${BASH_SOURCE[0]}" )" >/dev/null 2>&1 && pwd )"
source ${currentDir}/base.sh # Get variables from base script.



set -eo pipefail
VM_IP=$(_get_vm_ip)

echo "$SSH_USER"@"$VM_IP"
ssh -v "$SSH_USER"@"$VM_IP" -o StrictHostKeyChecking=no -f 'shutdown -s -t 00 -f'


# Destroy VM.
virsh shutdown "$VM_ID"

# Undefine VM.
virsh undefine "$VM_ID"

# Delete VM disk.
if [ -f "$VM_IMAGE" ]; then
    rm -f "$VM_IMAGE"
fi
