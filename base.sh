#!/usr/bin/env bash

# /opt/libvirt-driver/base.sh
export LIBVIRT_DEFAULT_URI="qemu:///system"
BASE_VM="$CUSTOM_ENV_BASE_VM.qcow2"
# BASE_VM="W10_GIT_NIPM.qcow2"
SSH_USER="SAS"
BASE_IMAGES_PATH="/mnt/myRAID5/base_images"
CLONED_IMAGES_PATH="/mnt/myRAID5/clones"
BASE_VM_IMAGE="$BASE_IMAGES_PATH/$BASE_VM"
VM_ID="runner-$CUSTOM_ENV_CI_RUNNER_ID-project-$CUSTOM_ENV_CI_PROJECT_PATH_SLUG-concurrent-$CUSTOM_ENV_CI_CONCURRENT_PROJECT_ID-job-$CUSTOM_ENV_CI_JOB_NAME_SLUG:$CUSTOM_ENV_CI_JOB_ID"
VM_IMAGE="$CLONED_IMAGES_PATH/$VM_ID.qcow2"

_get_vm_ip() {
    virsh -q domifaddr "$VM_ID" | awk '{print $4}' | sed -E 's|/([0-9]+)?$||'
}
